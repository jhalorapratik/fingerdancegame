## Summary ##

This is an android application. Its name is FingerDanceGame.

Its an multi player game. For small screen devices 2 player mode is only
available, but for large screen devices 2,3,4 player modes are available.

## How To Install ##

Method 1:

[1] Clone the repository and build the app.
[2] Install debug app via adb.


## How To Play ##

* Playing mode need to chosen to play.
* At start, any random tile will be highlighted and a user needs to press that
    tile. After that another tile will be highlighted and other user needs to
    press that tile.
* Similarly players need to tap tiles alternatively.
* For game to complete, all the tiles must be pressed and no user can lift any
    of the finger.
*  If any user lifts a finger from screen, before the game is completed, game
    will over.
 
## Description of Design ##

* This project comprise of one activity, two fragments, and one grid view
    adapter.
* dimensions files are used in layouts. This will automatically adjust sizes
    according to device types (Large / Small screen devices).

## Approach ##

* On App start, user is prompted to choose play mode.
* Using Display Metrics and input from user, column size is calculated.
* Most of the work is done on fragments. Hence this gives us flexibility to use
    the code and make it easy to maintain.
* Adapter is used to create single column views and all the functionality
    related to that.* 

## Credits ##
This project is made by Pratik Jhalora.