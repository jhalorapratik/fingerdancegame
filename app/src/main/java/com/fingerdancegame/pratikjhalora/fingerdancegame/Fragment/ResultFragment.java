package com.fingerdancegame.pratikjhalora.fingerdancegame.Fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fingerdancegame.pratikjhalora.fingerdancegame.Activity.MainActivity;
import com.fingerdancegame.pratikjhalora.fingerdancegame.R;

import java.util.Random;

/**
 * Created by pratik on 24/8/16.
 */
public class ResultFragment extends Fragment
{
    Activity activity;
    TextView ResultText, PlayAgainButton, CloseButton;
    Bundle bundle;
    SharedPreferences sharedPreferences;
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        activity = getActivity();
        sharedPreferences = activity.getSharedPreferences("GamePrefs", Context.MODE_PRIVATE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.result_show, container, false);
        ResultText = (TextView) rootView.findViewById(R.id.result_text);
        PlayAgainButton = (TextView) rootView.findViewById(R.id.play_again_button);
        CloseButton = (TextView) rootView.findViewById(R.id.close_button);
        bundle = new Bundle();
        bundle = getArguments();

        if (bundle.getString("result_text").equalsIgnoreCase("Fail"))
        {
            ResultText.setText("oops !!! Game Over");
        }
        else
        {
            ResultText.setText("Congratulations !!! You Won");
        }

        PlayAgainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                int SelectedNum = sharedPreferences.getInt("SelectedNum",3);
                int Scope = SelectedNum;
                int[] array = new int[Scope*Scope];
                for (int i=0 ; i < SelectedNum*SelectedNum ; i++)
                {
                    array[i] = i;
                }

                int RandomNum = pickARandomNumber(array);

                Bundle bundle = new Bundle();
                bundle.putInt("NumberOfItems",SelectedNum);
                bundle.putInt("RandomNum",RandomNum);
                android.app.FragmentManager fragmentManager = getFragmentManager();
                android.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                GameFragment gameFragment = new GameFragment();
                gameFragment.setArguments(bundle);
                fragmentTransaction.add(R.id.fragment_container,gameFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        CloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity,MainActivity.class);
                activity.startActivity(intent);
            }
        });
        return rootView;
    }

    public int pickARandomNumber(int[] array)
    {
        int rnd = new Random().nextInt(array.length);
        return array[rnd];
    }
}
