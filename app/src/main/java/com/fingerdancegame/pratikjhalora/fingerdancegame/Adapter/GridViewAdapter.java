package com.fingerdancegame.pratikjhalora.fingerdancegame.Adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fingerdancegame.pratikjhalora.fingerdancegame.R;

import java.util.Random;

/**
 * Created by pratik on 23/8/16.
 */
public class GridViewAdapter extends BaseAdapter
{
    private Context context;
    private Integer NumberOfColoumns;
    private int height, RandomNum;
    View gridView;
    LinearLayout ll;

    public GridViewAdapter(Context context, int NumberOfColoumns, int height, int RandomNum)
    {
        this.context = context;
        this.NumberOfColoumns = NumberOfColoumns;
        this.height = height;
        this.RandomNum = RandomNum;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
        {
            gridView = new View(context);

            gridView = inflater.inflate(R.layout.grid_item_layout, null);

            ll = (LinearLayout) gridView.findViewById(R.id.grid_item_linear_layout);

            ll.setLayoutParams(new LinearLayout.LayoutParams(height,height));

        } else
        {
            gridView = (View) convertView;
        }

        TextView textView = (TextView) gridView.findViewById(R.id.grid_item_layout);

        if (position == RandomNum)
        {
            int randomStr = new Random().nextInt(50);
            TypedArray ta = context.getResources().obtainTypedArray(R.array.colors);
            int colorToUse = ta.getResourceId(randomStr, 0);
            textView.setBackgroundColor(colorToUse);
            textView.setText(String.valueOf(position));
        }
        else
        {
            textView.setText(String.valueOf(position));
        }

        return gridView;
    }

    public void getRandomNumber(int number)
    {
        RandomNum = number;
        gridView.invalidate();
    }
    @Override
    public void notifyDataSetChanged()
    {
        super.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return NumberOfColoumns*NumberOfColoumns.shortValue();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
