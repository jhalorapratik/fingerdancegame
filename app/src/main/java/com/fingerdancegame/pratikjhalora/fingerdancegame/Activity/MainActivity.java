package com.fingerdancegame.pratikjhalora.fingerdancegame.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.fingerdancegame.pratikjhalora.fingerdancegame.Fragment.GameFragment;
import com.fingerdancegame.pratikjhalora.fingerdancegame.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    Activity activity;
    TextView PlayButton;
    Spinner spinner;
    HashMap<String,Integer> map;
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activity = this;
        sharedPreferences = getSharedPreferences("GamePrefs", Context.MODE_PRIVATE);
        PlayButton = (TextView) findViewById(R.id.play_now_button);
        spinner = (Spinner) findViewById(R.id.board_spinner);
        map = new HashMap<String, Integer>();
        final int number = FindNumOfItemsToFitInRow();
        List<String> list = new ArrayList<String>();

        if (isTablet(activity))
        {
            for (int i=2 ; i <= number ; i++)
            {
                if (i == 2 || i == 3)
                {
                    String s;
                    list.add(i+" x "+i+"   -   2 Players");
                    map.put(i+" x "+i+"   -   2 Players",i);
                }
                else if (i==4 || i==5)
                {
                    list.add(i+" x "+i+"   -   3 Players");
                    map.put(i+" x "+i+"   -   3 Players",i);
                }
                else if (i == 6)
                {
                    list.add(i+" x "+i+"   -   4 Players");
                    map.put(i+" x "+i+"   -   4 Players",i);
                }
            }
        }
        else
        {
            for (int i=2 ; i <= 3 ; i++)
            {
                list.add(i+" x "+i+"   -   2 Players");
                map.put(i+" x "+i+"   -   2 Players",i);
            }
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);

        PlayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                int SelectedNum = map.get(spinner.getSelectedItem());
                int Scope = map.get(spinner.getSelectedItem());
                int[] array = new int[Scope*Scope];
                for (int i=0 ; i < SelectedNum*SelectedNum ; i++)
                {
                    array[i] = i;
                }

                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putInt("SelectedNum",SelectedNum);
                editor.commit();

                Bundle bundle = new Bundle();
                bundle.putInt("NumberOfItems",map.get(spinner.getSelectedItem()));
                android.app.FragmentManager fragmentManager = getFragmentManager();
                android.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                GameFragment gameFragment = new GameFragment();
                gameFragment.setArguments(bundle);
                fragmentTransaction.add(R.id.fragment_container,gameFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
    }

    public int FindNumOfItemsToFitInRow()
    {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        int num = (int) ((double)width / 120);
        return num;
    }

    public int pickARandomNumber(int[] array)
    {
        int rnd = new Random().nextInt(array.length);
        return array[rnd];
    }

    public boolean isTablet(Context context)
    {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }
}
