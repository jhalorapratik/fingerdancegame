package com.fingerdancegame.pratikjhalora.fingerdancegame.Fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.fingerdancegame.pratikjhalora.fingerdancegame.Adapter.GridViewAdapter;
import com.fingerdancegame.pratikjhalora.fingerdancegame.R;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by pratik on 23/8/16.
 */
public class GameFragment extends Fragment
{
    Activity activity;
    GridView gridView;
    public int NumberOfColoumns, PreviousNum=0, ItemBlockHeight, RandomNumber, Counter = 1;
    boolean check = false;
    ArrayList<Integer> array, arrayListDB;
    GridViewAdapter adapter;
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        activity = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.grid_layout, container, false);

        gridView = (GridView) rootView.findViewById(R.id.grid_layout);

        Bundle bundleOne = getArguments();
        NumberOfColoumns = bundleOne.getInt("NumberOfItems");
        array = new ArrayList<Integer>();
        arrayListDB = new ArrayList<Integer>();

        array = new ArrayList<Integer>();
        for (int i=0 ; i < NumberOfColoumns*NumberOfColoumns ; i++)
        {
            array.add(i);
        }

        RandomNumber = pickARandomNumber(array);

        gridView.setNumColumns(NumberOfColoumns);

        ItemBlockHeight = loadImageWithHeightAndWidth();
        adapter = new GridViewAdapter(activity, NumberOfColoumns, ItemBlockHeight, RandomNumber);
        gridView.setAdapter(adapter);

        gridView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent)
            {
                boolean returnValue;

                if (PreviousNum == 0)
                {
                    PreviousNum = motionEvent.getPointerCount();
                }

                if ((motionEvent.getAction() == MotionEvent.ACTION_DOWN))
                {
                    returnValue = true;
                }
                else if ((motionEvent.getAction() == MotionEvent.ACTION_UP))
                {
                    GoToResultFragment("Fail");
                    returnValue = true;
                }
                else returnValue = false;

                if (PreviousNum > motionEvent.getPointerCount() && motionEvent.getAction() != MotionEvent.ACTION_MOVE)
                {
                    if (check == false)
                    {
                        GoToResultFragment("Fail");
                    }

                }
                else if (motionEvent.getAction() != MotionEvent.ACTION_MOVE)
                {
                    if (arrayListDB.size() == NumberOfColoumns*NumberOfColoumns )
                    {
                        check = true;
                        if (motionEvent.getPointerCount() == NumberOfColoumns*NumberOfColoumns)
                        {
                            GoToResultFragment("Pass");
                        }else
                        {
                            GoToResultFragment("Fail");
                        }
                    }
                    else if (Counter == motionEvent.getPointerCount())
                    {

                        check =true;
                        PreviousNum = motionEvent.getPointerCount();
                        RandomNumber = pickARandomNumber(array);
                        adapter.getRandomNumber(RandomNumber);
                        adapter.notifyDataSetChanged();
                        gridView.invalidateViews();
                        Counter ++;
                    }
                }

                return returnValue;
            }
        });
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id)
            {
                Toast.makeText(activity, String.valueOf(position), Toast.LENGTH_SHORT).show();
            }
        });

        return rootView;
    }

    public int loadImageWithHeightAndWidth()
    {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        int ratio = (int) ((double)width / NumberOfColoumns);
        return ratio;
    }

    public int pickARandomNumber(ArrayList<Integer> array)
    {
        int rnd = 0;
        for (int i=0 ; i < array.size() ; i++)
        {
            rnd = new Random().nextInt(array.size());

            if (!arrayListDB.contains(rnd))
            {
                arrayListDB.add(rnd);
                break;
            }
        }

        return rnd;
    }

    public void GoToResultFragment(String action)
    {
        Bundle bundle = new Bundle();
        bundle.putString("result_text",action);
        android.app.FragmentManager fragmentManager = getFragmentManager();
        android.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        ResultFragment resultFragment = new ResultFragment();
        resultFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.fragment_container,resultFragment);
        fragmentTransaction.commit();
    }
}
